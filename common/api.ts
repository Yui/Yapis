// deno-lint-ignore no-explicit-any
export type genericFunction<> = (config: Record<string, any>) => any;

export interface path<T extends genericFunction = genericFunction, N extends string = string> {
  suffix: string;
  method?: string;
  /** Used for typing needed parameters and right return type. */
  fn: T;
  name: N;
  /** Headers per path. Will get merged with global api headers. Path ones override global ones. */
  headers?: HeadersInit;
  /** For get method. Replace {id} with values. */
  URLParams?: boolean;
}

export class APIError extends Error {
  constructor(response: Response) {
    super(response.statusText);
    this.name = "APIError";
    this.status = response.status;
    this.headers = response.headers;
    this.url = response.url;
  }
  status: number;
  headers: Headers;
  url: string;
}

export class api<U extends Readonly<Record<string, path>>> {
  baseURL: string;
  /** Global headers for all paths. */
  headers?: HeadersInit;
  paths: U;

  constructor(
    config: { protocol: string; hostname: string; port?: number; path?: string; headers?: HeadersInit },
    paths: U
  ) {
    this.baseURL = `${config.protocol}://${config.hostname}${config.port ? `:${config.port}` : ""}${config.path ?? ""}`;
    this.paths = paths;
    this.headers = config.headers;
  }

  async exec<O extends keyof U = keyof U>(name: O, params: Parameters<U[O]["fn"]>[0]): Promise<ReturnType<U[O]["fn"]>> {
    const path = this.paths[name];
    if (!path) throw new TypeError("Invalid name");

    if (params) {
      for (const key of Object.keys(params)) {
        if ([null, undefined].includes(params[key])) delete params[key];
      }
    }

    let url = `${this.baseURL}${path.suffix}`;

    const parsedParams: Record<string, string> = {};

    if (path.URLParams) {
      for (const key of Object.keys(params)) {
        if (url.includes(`{${key}}`)) url = url.replaceAll(`{${key}}`, params[key]);
        else parsedParams[key] = params[key];
      }

      if (path.method === "GET" && Object.keys(parsedParams).length > 0)
        url += "?" + new URLSearchParams(parsedParams).toString();
    } else if (path.method === "GET") url += "?" + new URLSearchParams(params).toString();

    // Combine headers
    const combinedHeaders = new Headers(this.headers ?? path.headers);
    if (this.headers && path.headers) {
      const pathHeaders = new Headers(path.headers);
      pathHeaders.forEach((val, key) => {
        combinedHeaders.set(key, val);
      });
    }

    const data = await fetch(url, {
      method: path.method,
      body: path.method === "POST" ? new URLSearchParams(params) : undefined,
      headers: combinedHeaders,
    });

    if (data.status !== 200) throw new APIError(data);

    if (data.headers.get("content-type")?.includes("application/json")) return await data.json();
    // deno-lint-ignore no-explicit-any
    else if (data.headers.get("content-type") == "image/png") return data as any;
    // deno-lint-ignore no-explicit-any
    else return (await data.text()) as any;
  }
}

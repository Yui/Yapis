import { TokenBucket } from "https://x.nest.land/Yutils@2.1.2/modules/tokenbucket.ts";
import { api } from "../../common/api.ts";
export { APIError } from "../../common/api.ts";

interface imageChartsDataset {
  /** Color of the background. https://documentation.image-charts.com/chart.js/#background-color */
  backgroundColor: string;
  /** rgb(54,162,235) */
  borderColor: string;
  borderWidth: number;
  data: number[];
  label: string;
}

/** Warning: This type is nowhere near complete. */
interface imageChartsDataLabels {
  type: "bar" | "line";
  data: {
    labels: string[];
    datasets: imageChartsDataset[];
  };
  options: {
    legend: {
      display: boolean;
    };
  };
}

export class imagecharts {
  private controller;

  private bucket: TokenBucket | undefined;

  /** Default: true */
  constructor(respectRateLimit?: boolean) {
    if (respectRateLimit != false) this.bucket = new TokenBucket(1, 1000, 1);
    this.controller = new api(
      {
        protocol: "https",
        hostname: "image-charts.com",
        path: "",
      },
      {
        graph: {
          name: "graph",
          suffix: "/chart.js/2.8.0",
          method: "GET",
          // All of the parameters are required
          // deno-lint-ignore no-unused-vars
          fn: (config: { width?: number; height?: number; bkg?: string; c?: string }) => {
            return {} as Response;
          },
        },
      }
    );
  }

  /** Link - link to image, Image - actual image. Just using link kinda bypasses rate limit. */
  async graph<T extends "link" | "image", E extends "blob" | "response" = "response">(
    mode: T,
    config: {
      width: number;
      height: number;
      /** Color of the background. https://documentation.image-charts.com/chart.js/#background-color */
      bkg: string;
      c: imageChartsDataLabels;
    },
    /** "blob" | "response". Default: response. */
    returnMode?: T extends "link" ? never : E
  ): Promise<T extends "link" ? string : E extends "response" ? Response : Blob> {
    if (mode === "link") {
      return `https://image-charts.com/chart.js/2.8.0?${new URLSearchParams({
        width: config.width.toString(),
        height: config.height.toString(),
        bkg: config.bkg,
        c: JSON.stringify(config.c),
        // deno-lint-ignore no-explicit-any
      })}` as any;
    }

    if (this.bucket) {
      if (!this.bucket.take()) throw new Error("Rate limited");
    }

    const data = await this.controller.exec("graph", {
      width: config.width,
      height: config.height,
      bkg: config.bkg,
      c: JSON.stringify(config.c),
    });

    if (data.ok && data.headers.get("Content-Type") == "image/png") {
      if (returnMode === "blob") {
        // deno-lint-ignore no-explicit-any
        return (await data.blob()) as any;
      } // deno-lint-ignore no-explicit-any
      return data as any;
    } else throw new Error(`Image failed: ${data}`);
  }
}

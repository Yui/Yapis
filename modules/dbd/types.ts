import { ranks } from "./mod.ts";

export interface character {
  id: string;
  name: string;
  role: "killer" | "survivor";
  difficulty: "string"; //"intermediate"
  gender: string; //"male"
  bio: string;
  story: string;
  /** Things like terror radius. power values... */
  tunables: Record<string, number>;
  /** Killer power item */
  item: string;
  /** Outfit ids */
  outift: string[];
  /** Codename or dlc */
  dlc: string;
  /** path to image. No clue how to use this???? */
  image: string;
}

export interface perk {
  /** Perk type */
  categories: [string];
  name: string;
  description: string;
  role: "survivor" | "killer";
  /** id of the character it belongs to. Null = general survivor/killer perk */
  character: number | null;
  /** no idea ? */
  tunables: string[][];
  /** no idea ? */
  modifier: string;
  /** Teachable unlock level. 0 for general survivor/killer perks */
  teachable: number;
  /** path to image. No clue how to use this???? */
  image: string;
}

export type grades = {
  rank: ranks;
  tier: 1 | 2 | 3 | 4;
};

export interface player {
  steamid: number;
  /** Total amount of bloodpoints earned */
  bloodpoints: number;
  /** Current survivor rank */
  survivor_rank: number;
  /** Trials of survivor with full loadout */
  survivor_fullloadout: number;
  /** Trials of survivor with perfect score ??? not sure about this */
  survivor_perfectgames: number;
  /** Ultra rare offerings burned as survivor */
  survivor_ultrarare: number;
  /** Amount of generator repaired */
  gensrepaired: number;
  /** Amount of generators that were damaged by killer repaired */
  damagedgensrepaired: number;
  /** Amount of survivors healed */
  survivorshealed: number;
  /** Amount of healed survivors while injured */
  survivorshealed_whileinjured: number;
  skillchecks: number;
  /** Total Amount of saved survivors. (dying state/unhooked) */
  saved: number;
  /** Amount of saved survivors in egc . (dying state/unhooked) */
  saved_endgame: number;
  /** Total Amount of escapes from trial. */
  escaped: number;
  /** Amount of escapes while in dying state */
  escaped_ko: number;
  /** Amount of escapes after unhooking yourself */
  hooked_escape: number;
  /** Amount of escapes through hatch */
  escaped_hatch: number;
  /** Amount of escapes through hatch by crawling */
  escaped_hatchcrawling: number;
  /** Amount of trials where everyone escaped through hatch */
  escaped_allhatch: number;
  /** Escapes while being downed at most once */
  escaped_downedonce: number;
  /** Escapes while being injured for over half a trial */
  escaped_injuredhalfoftrail: number;
  /** Escapes as obsession without getting hit */
  escaped_nobloodlossobsession: number;
  /** Escapes after fixing last generator as lone survivor */
  escaped_lastgenlastsurvivor: number;
  /** Escapes with a new item */
  escaped_newitem: number;
  /** Escapes with somebody else's item */
  escaped_withitemfrom: number;
  /** Prot hits near hook */
  protectionhits_unhooked: number;
  /** Prot hits for carried survivors */
  protectionhits_whilecarried: number;
  /** Amount of survivors healed from dying state. */
  healeddyingtoinjured: number;
  /** Amount of obsessions healed */
  obsessionshealed: number;
  /** Amount of depleted items */
  itemsdepleted: number;
  /** No idea */
  survivorshealed_threenothealthy: number;
  /** Healed survivors that come to you first */
  survivorshealed_foundyou: number;
  /** Survivors saved by pallet from killer. */
  killerstunnedpalletcarrying: number;
  /** Amount of generators repaired without using any perk */
  gensrepaired_noperks: number;
  /** Chases escaped by pallet stunning killer */
  escapedchase_palletstun: number;
  /** Chases escaped after getting injured during a chase */
  escapedchase_healthyinjured: number;
  /** Chases escaped by hiding in a locker */
  escapedchase_hidinginlocker: number;
  /** Basic Attacks and Projectiles dodged */
  dodgedattack: number;
  /** Vaults while chased */
  vaultsinchase: number;
  /** Hits evaed by vaulting */
  vaultsinchase_missed: number;
  /** Escapes from killers graps */
  wiggledfromkillersgrasp: number;
  /** Hex totems cleansed */
  hextotemscleansed: number;
  /** Hex totems blessed */
  hextotemsblessed: number;
  /** Totems blessed */
  blessedtotemboosts: number;
  /** Opened exist gates */
  exitgatesopened: number;
  /** Self unhooks */
  unhookedself: number;
  /** Hook sabotages */
  hookssabotaged: number;
  /** Chest searches */
  chestssearched: number;
  /** Chest searches in basement*/
  chestssearched_basement: number;
  /** Bloodweb mystery boxes opened */
  mysteryboxesopened: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_disturbedward: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_fathercampbellschapel: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_mothersdwelling: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_templeofpurgation: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_game: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_familyresidence: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_sanctumofwrath: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_mountormondresort: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_lampkinlane: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_palerose: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_undergroundcomplex: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_treatmenttheatre: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_deaddawgsaloon: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_midwichelementaryschool: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_racconcitypolicestation: number;
  /** Map escapes after fixing 2nd floor gen */
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_eyrieofcrows: number;
  /** Map escapes after fixing 2nd floor gen */
  secondfloorgen_gardenofjoy: number;
  /** Current killer rank */
  killer_rank: number;
  /** Trials of killer with full loadout */
  killer_fullloadout: number;
  /** Trials of killer with perfect score ??? not sure about this  */
  killer_perfectgames: number;
  /** Ultra rare offerings burned as killer */
  killer_ultrarare: number;
  /** Amount of survivors morried */
  killed: number;
  /** Trials where all survivors died from Tier III Evil Within (Myers/Shape) */
  killed_allevilwithin: number;
  /** Sacrifices */
  sacrificed: number;
  /** Trials where all survivors died before egc */
  sacrificed_allbeforelastgen: number;
  /** Obsessions sacrificed */
  sacrificed_obsessions: number;
  /** Sacrifices/Kills during egc */
  killed_sacrificed_afterlastgen: number;
  /** (The Nurse) Blink attacks */
  blinkattacks: number;
  /** (The Hillbilly) chainsaw downs */
  chainsawhits: number;
  /** (The Shape) Evil within tier ups */
  evilwithintierup: number;
  /** (The Trapper) Survivors trapped and picked up */
  beartrapcatches: number;
  /** (The Wraith) Uncloak attacks */
  uncloakattacks: number;
  /** (The Doctor) Shocks */
  shocked: number;
  /** (The Huntress) Hatchets thrown */
  hatchetsthrown: number;
  /** (The Nightmare) Survivors put to sleep */
  dreamstate: number;
  /** (The Pig) Reverse bear-traps put on survivors */
  rbtsplaced: number;
  /** (The Hag) Traps triggered */
  phantasmstriggered: number;
  /** (The Executioner) Survivors sent to Cage of Atonement:  */
  cagesofatonement: number;
  /** (The Blight) Survivors hit with Lethal Rush */
  lethalrushhits: number;
  /** (The Trickster) Max Lacerations dealt */
  lacerations: number;
  /** (The Cenobite) Survivors bound with a possessed chain */
  possessedchains: number;
  /** (The Onryō) Condemned survivors */
  condemned: number;
  /** Generators damaged with at least one survivor hooked */
  gensdamagedwhileonehooked: number;
  /** Generators damaged while Undetectable */
  gensdamagedwhileundetectable: number;
  /** Survivors grabbed while repairing a generator */
  survivorsgrabbedrepairinggen: number;
  /** Grabbed survivors hiding inside a locker */
  survivorsgrabbedfrominsidealocker: number;
  /** (The Doctor) Trials where all survivors suffered from madness 3 */
  survivorsallmaxmadness: number;
  /** (The Hag) Trials where each survivor was hit after teleport */
  survivorshiteachafterteleporting: number;
  /** Survivors hit after they drop a pallet during chase */
  survivorshitdroppingpalletinchase: number;
  /** Survivors hit while carrying another survivor: */
  survivorshitwhilecarrying: number;
  /** Hooked 3+ survivors in the basement at once */
  survivorsthreehookedbasementsametime: number;
  /** Hatches closed */
  hatchesclosed: number;
  /** Hooked survivors while all remaining were injured: */
  hookedwhilethreeinjured: number;
  /** Survivors interrupted while cleansing a totem */
  survivorsinterruptedcleansingtotem: number;
  /** Hooked survivors during egc */
  survivorshookedendgamecollapse: number;
  /** Hooked survivors before a single generator is repaired */
  survivorshookedbeforegenrepaired: number;
  /** Hooked survivors in basement: */
  survivorshookedinbasement: number;
  /** Survivors downed while carrying another survivor */
  survivorsdownedwhilecarrying: number;
  /** Survivors downed next to raised pallets */
  survivorsdownednearraisedpallet: number;
  /** (The Huntress) Survivors downed by 24m+ hatchet throw */
  survivorsdowned_hatchets: number;
  /** (The Cannibal) Survivors downed with chainsaw */
  survivorsdowned_chainsaw: number;
  /** (The Clown) Intoxicated survivors downed */
  survivorsdowned_intoxicated: number;
  /** (The Spirit) Survivors downed after phase walk */
  survivorsdowned_haunting: number;
  /** (The Legion) Deep wounded survivors downed */
  survivorsdowned_deepwound: number;
  /** (The Plague) Survivors downed while having max Sickness */
  survivorsdowned_maxsickness: number;
  /** (The Ghostface) Marked survivors downed */
  survivorsdowned_marked: number;
  /** The Demogorgon Survivors downed with Shred */
  survivorsdowned_shred: number;
  /** (The Oni) Survivors downed with Demon Fury */
  survivorsdowned_bloodfury: number;
  /** (The Gunslinger) Survivors downed while speared */
  survivorsdowned_speared: number;
  /** (The Twins) Survivors downed with Victor clingin */
  survivorsdowned_victor: number;
  /** (The Nemesis) Survivors downed while contaminated */
  survivorsdowned_contaminated: number;
  /** (The Artist) Survivors downed with Dire Crows */
  survivorsdowned_direcrows: number;
  /** (The Dredge) Survivors downed during Nightfall */
  survivorsdowned_nightfall: number;
  /** Oblivious survivors downed */
  survivorsdowned_oblivious: number;
  /** Survivors downed while Exposed */
  survivorsdowned_exposed: number;
  /** no idea */
  hash: string;
  /** last data update */
  updated_at: number;
  created_at: number;
  /** Playtime in minutes */
  playtime: number;
}

export type betterPlayer = Omit<player, "survivor_rank" | "killer_rank"> & {
  /** Current survivor rank */
  survivor_rank: grades;
  /** Current kilelr rank */
  killer_rank: grades;
};

export interface shrine {
  id: number;
  perks: { id: string; bloodpoints: number; shards: number }[];
  start: number;
  end: number;
}

export type betterShrine = Omit<shrine, "start" | "end"> & {
  start: Date;
  end: Date;
};

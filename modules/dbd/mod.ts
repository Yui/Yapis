import { AtLeastOne } from "https://x.nest.land/Yutils@2.1.2/common/atLeastOne.ts";
import { TokenBucket } from "https://x.nest.land/Yutils@2.1.2/modules/tokenbucket.ts";
import { api } from "../../common/api.ts";
import type { betterPlayer, betterShrine, character, grades, perk, player, shrine } from "./types.ts";
export { APIError } from "../../common/api.ts";
export * from "./types.ts";

export enum ranks {
  iridescent = 1,
  gold = 2,
  silver = 3,
  bronze = 4,
  iron = 5,
}

export class dbd {
  private controller;

  private bucket: TokenBucket | undefined;

  /** Default: true, Rate limits are not said so we are going with 2/5s */
  constructor(
    config?: AtLeastOne<{
      respectRateLimit?: boolean;
      customRateLimit?: {
        /** Max bucket capacity Default: 20 */
        bucketCapacity: number;
        /** How often a token gets added. Default: 1000 */
        bucketRefillInterval: number;
        /** How much tokens get added. Default: 20 */
        amountPerBucketRefill: number;
      };
    }>
  ) {
    if (config?.respectRateLimit != false)
      this.bucket = new TokenBucket(
        config?.customRateLimit?.bucketCapacity ?? 20,
        config?.customRateLimit?.bucketRefillInterval ?? 1000,
        config?.customRateLimit?.amountPerBucketRefill ?? 20
      );

    this.controller = new api(
      {
        protocol: "https",
        hostname: "dbd.tricky.lol",
        path: "/api",
      },
      {
        characters: {
          name: "characters",
          suffix: "/characters",
          method: "GET",
          fn: () => {
            return {} as Record<string, character>;
          },
        },
        perks: {
          name: "perks",
          suffix: "/perks",
          method: "GET",
          fn: () => {
            return {} as Record<string, perk>;
          },
        },
        perk: {
          name: "perk",
          suffix: "/perkinfo",
          method: "GET",
          // Perk is required!
          // TODO: Figure out a way to make ts allow required values here
          // deno-lint-ignore no-unused-vars
          fn: (config: { perk?: string }) => {
            return {} as perk;
          },
        },
        player: {
          name: "player",
          suffix: "/playerstats",
          method: "GET",
          // Steam id is required!
          // TODO: Figure out a way to make ts allow required values here
          // deno-lint-ignore no-unused-vars
          fn: (config: { steamid?: string }) => {
            return {} as player;
          },
        },
        shrine: {
          name: "shrine",
          suffix: "/shrine",
          method: "GET",
          fn: () => {
            return {} as shrine;
          },
        },
        rankreset: {
          name: "rankreset",
          suffix: "/rankreset",
          method: "GET",
          fn: () => {
            return {} as { rankreset: number };
          },
        },
      }
    );
  }

  async characters() {
    this.bucket?.tryTake();
    const data = await this.controller.exec("characters", undefined);
    return Object.values(data);
  }

  async perks() {
    this.bucket?.tryTake();
    const data = await this.controller.exec("perks", undefined);
    return Object.values(data);
  }

  async perk(perk: string) {
    this.bucket?.tryTake();
    const data = await this.controller.exec("perk", { perk: perk });
    return data;
  }

  /** Statistics of a player. Steam privacy settings can prevent this from working. */
  async player(steamid: string): Promise<betterPlayer> {
    if (!/^-?\d+$/.test(steamid)) throw new Error("Steam id should be numeric");
    this.bucket?.tryTake();
    const data = await this.controller.exec("player", { steamid: steamid });

    return {
      ...data,
      killer_rank: this.gradeConverter(data.killer_rank),
      survivor_rank: this.gradeConverter(data.survivor_rank),
    };
  }

  protected between(value: number, min: number, max: number, includeEdge?: boolean) {
    return includeEdge ? value >= min && value <= max : value > min && value < max;
  }

  protected gradeConverter(rankNumber: number): grades {
    /* Iri 1 - 1
    Iri 2 - 2
    Iri 3 - 3
    Iri 4 - 4
    Gold 1 - 5
    Gold 2 - 6
    Gold 3 - 7
    Gold 4 - 8
    Silver 1 - 9
    Silver 2 - 10
    Silver 3 - 11
    Silver 4 - 12
    Bronze 1 - 13
    Bronze 2 - 14
    Bronze 3 - 15
    Bronze 4 - 16
    Iron 1 - 17
    Iron 2 - 18
    Iron 3 - 19
    Iron 4 - 20*/
    const tmp = rankNumber / 4;
    const rank: ranks = this.between(tmp, 1, 4)
      ? 1
      : this.between(tmp, 5, 8)
      ? 2
      : this.between(tmp, 9, 12)
      ? 3
      : this.between(tmp, 13, 16)
      ? 4
      : 5;

    let tier = rankNumber % 4;
    if (tier == 0) tier = 4;

    return {
      rank: rank,
      // deno-lint-ignore no-explicit-any
      tier: tier as any,
    };
  }

  async shrine(): Promise<betterShrine> {
    this.bucket?.tryTake();
    const data = await this.controller.exec("shrine", undefined);
    return {
      ...data,
      start: new Date(data.start * 1000),
      end: new Date(data.end * 1000),
    };
  }

  async rankreset() {
    this.bucket?.tryTake();
    const data = await this.controller.exec("rankreset", undefined);
    return new Date(data.rankreset * 1000);
  }
}

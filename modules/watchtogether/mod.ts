import { api } from "../../common/api.ts";
export { APIError } from "../../common/api.ts";

export class watch2gether {
  private controller;
  constructor() {
    this.controller = new api(
      {
        protocol: "https",
        hostname: "api.w2g.tv",
        path: "/rooms",
      },
      {
        create: {
          name: "create",
          suffix: "/create.json",
          method: "POST",
          fn: () => {
            return {} as {
              id: number;
              streamkey: string;
              created_at: string;
              persistent: boolean;
              persistent_name: string;
              deleted: boolean;
              moderated: boolean;
              location: string;
              stream_created: boolean;
              background: null;
              moderated_background: boolean;
              moderated_playlist: boolean;
              bg_color: string;
              bg_opacity: number;
              moderated_item: boolean;
              theme_bg: string;
              playlist_id: number;
              members_only: boolean;
              moderated_suggestions: boolean;
              moderated_chat: boolean;
              moderated_user: boolean;
              moderated_cam: boolean;
            };
          },
        },
      }
    );
  }

  async createRoom() {
    const room = await this.controller.exec("create", undefined);
    return {
      ...room,
      link: `https://w2g.tv/rooms/${room.streamkey}`,
    };
  }
}

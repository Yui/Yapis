import { api } from "../../common/api.ts";
import { continents, countries, states } from "./enums.ts";
export { APIError } from "../../common/api.ts";

export class diseasesh {
  private controller;

  constructor(config?: { baseURL?: "disease.sh" | "api.caw.sh" | "corona.lmao.ninja" }) {
    this.controller = new api(
      {
        protocol: "https",
        hostname: config?.baseURL ?? "disease.sh",
        path: "/v3/covid-19",
      },
      {
        all: {
          name: "all",
          suffix: "/all",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config?: {
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle */
            twoDaysAgo?: number;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return {} as {
              updated: number;
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              todayRecovered: number;
              active: number;
              critical: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
              oneCasePerPeople: number;
              oneDeathPerPeople: number;
              oneTestPerPeople: number;
              activePerOneMillion: number;
              recoveredPerOneMillion: number;
              criticalPerOneMillion: number;
              affectedCountries: number;
            };
          },
        },
        states: {
          name: "states",
          suffix: "/states",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            /** result will be sorted from greatest to least */
            sort?: "causes" | "todayCases" | "deaths" | "active";
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return [] as {
              state: string;
              updated: number;
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              active: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
            }[];
          },
        },
        state: {
          name: "state",
          suffix: "/states/{state}",
          method: "GET",
          URLParams: true,
          // TODO: Figure out a way to make ts allow required values here
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            /** US State */
            state?: string;
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return {} as {
              state: string;
              updated: number;
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              active: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
            };
          },
        },
        continents: {
          name: "continents",
          suffix: "/continents",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle */
            twoDaysAgo?: number;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return [] as {
              updated: number;
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              todayRecovered: number;
              active: number;
              critical: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
              continent: string;
              activePerOneMillion: number;
              recoveredPerOneMillion: number;
              criticalPerOneMillion: number;
              continentInfo: { lat: number; long: number };
              countries: [string];
            }[];
          },
        },
        continent: {
          name: "continent",
          suffix: "/continents/{continent}",
          method: "GET",
          URLParams: true,
          // TODO: Figure out a way to make ts allow required values here
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            continent?: string;
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle */
            twoDaysAgo?: number;
            /** False = fuzzy search */
            strict?: boolean;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return {} as {
              updated: number;
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              todayRecovered: 0;
              active: number;
              critical: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
              continent: string;
              activePerOneMillion: number;
              recoveredPerOneMillion: number;
              criticalPerOneMillion: number;
              continentInfo: { lat: number; long: number };
              countries: [string];
            };
          },
        },
        countries: {
          name: "countries",
          suffix: "/countries",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config?: {
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle */
            twoDaysAgo?: number;
            /** result will be sorted from greatest to least */
            sort?: "cases" | "todayCases" | "deaths" | "recovered" | "active";
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return [] as {
              updated: number;
              country: string;
              countryInfo: {
                _id: number;
                iso2: string;
                iso3: string;
                lat: number;
                long: number;
                flag: string;
              };
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              todayRecovered: number;
              active: number;
              critical: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
              continent: string;
              oneCasePerPeople: number;
              oneDeathPerPeople: number;
              oneTestPerPeople: number;
              activePerOneMillion: number;
              recoveredPerOneMillion: number;
              criticalPerOneMillion: number;
            }[];
          },
        },
        country: {
          name: "country",
          suffix: "/countries/{country}",
          method: "GET",
          URLParams: true,
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            country?: string;
            /** 0/1 toggle */
            yesterday?: number;
            /** 0/1 toggle */
            twoDaysAgo?: number;
            /** False = fuzzy search */
            strict?: boolean;
            /** 0/1 toggle - returns null instead of 0 when on*/
            allowNull?: number;
          }) => {
            return {} as {
              updated: number;
              country: string;
              countryInfo: {
                _id: number;
                iso2: string;
                iso3: string;
                lat: number;
                long: number;
                flag: string;
              };
              cases: number;
              todayCases: number;
              deaths: number;
              todayDeaths: number;
              recovered: number;
              todayRecovered: number;
              active: number;
              critical: number;
              casesPerOneMillion: number;
              deathsPerOneMillion: number;
              tests: number;
              testsPerOneMillion: number;
              population: number;
              continent: string;
              oneCasePerPeople: number;
              oneDeathPerPeople: number;
              oneTestPerPeople: number;
              activePerOneMillion: number;
              recoveredPerOneMillion: number;
              criticalPerOneMillion: number;
            };
          },
        },
        history: {
          name: "history",
          suffix: "/historical",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: { lastdays?: number }) => {
            return [] as {
              country: string;
              province: [string];
              timeline: {
                description: string;
                cases: Record<string, number>;
                deaths: Record<string, number>;
                recovered: Record<string, number>;
              };
            }[];
          },
        },
        countryHistory: {
          name: "countryHistory",
          suffix: "/historical/{country}",
          URLParams: true,
          method: "GET",
          // Country is required
          // deno-lint-ignore no-unused-vars
          fn: (config: { country?: string; lastdays?: number }) => {
            return {} as {
              country: string;
              province: [string];
              timeline: {
                description: string;
                cases: Record<string, number>;
                deaths: Record<string, number>;
                recovered: Record<string, number>;
              };
            };
          },
        },
      }
    );
  }

  async all(day?: "yesterday" | "dbfyesterday", allowNull?: boolean) {
    const data = await this.controller.exec("all", {
      yesterday: day === "yesterday" ? 1 : undefined,
      twoDaysAgo: day === "dbfyesterday" ? 1 : undefined,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async states(yesterday?: boolean, sort?: "causes" | "todayCases" | "deaths" | "active", allowNull?: boolean) {
    const data = await this.controller.exec("states", {
      sort: sort,
      yesterday: yesterday == true ? 1 : undefined,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async state(state: keyof typeof states, yesterday?: boolean, allowNull?: boolean) {
    const data = await this.controller.exec("state", {
      state: state,
      yesterday: yesterday == true ? 1 : undefined,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async continents(day?: "yesterday" | "dbfyesterday", allowNull?: boolean) {
    const data = await this.controller.exec("continents", {
      yesterday: day === "yesterday" ? 1 : undefined,
      twoDaysAgo: day === "dbfyesterday" ? 1 : undefined,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async continent(
    continent: keyof typeof continents,
    day?: "yesterday" | "dbfyesterday",
    strict?: boolean,
    allowNull?: boolean
  ) {
    const data = await this.controller.exec("continent", {
      continent: continent,
      yesterday: day === "yesterday" ? 1 : undefined,
      twoDaysAgo: day === "dbfyesterday" ? 1 : undefined,
      strict: strict,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async countries(
    day?: "yesterday" | "dbfyesterday",
    sort?: "cases" | "todayCases" | "deaths" | "recovered" | "active",
    allowNull?: boolean
  ) {
    const data = await this.controller.exec("countries", {
      yesterday: day === "yesterday" ? 1 : undefined,
      twoDaysAgo: day === "dbfyesterday" ? 1 : undefined,
      sort: sort,
      allowNull: allowNull == true ? 1 : undefined,
    });

    return data;
  }

  async country(
    country: keyof typeof countries,
    day?: "yesterday" | "dbfyesterday",
    strict?: boolean,
    allowNull?: boolean
  ) {
    const data = await this.controller.exec("country", {
      country: country,
      yesterday: day === "yesterday" ? 1 : undefined,
      twoDaysAgo: day === "dbfyesterday" ? 1 : undefined,
      strict: strict,
      allowNull: allowNull == true ? 1 : undefined,
    });
    return data;
  }

  async history(days?: number) {
    const data = await this.controller.exec("history", { lastdays: days });
    return data;
  }

  async countryHistory(country: keyof typeof countries, days?: number) {
    const data = await this.controller.exec("countryHistory", {
      country: country,
      lastdays: days,
    });
    return data;
  }
}

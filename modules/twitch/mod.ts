import { AtLeastOne } from "https://x.nest.land/Yutils@2.1.2/common/atLeastOne.ts";
import { TokenBucket } from "https://x.nest.land/Yutils@2.1.2/modules/tokenbucket.ts";
import { api } from "../../common/api.ts";
export { APIError } from "../../common/api.ts";

export class twitch {
  private controller;

  private bucket: TokenBucket | undefined;

  /** Default: true, Rate limits are not said so we are going with 2/5s */
  constructor(
    config: AtLeastOne<{
      respectRateLimit?: boolean;
      customRateLimit?: {
        /** Max bucket capacity Default: 20 */
        bucketCapacity: number;
        /** How often a token gets added. Default: 1000 */
        bucketRefillInterval: number;
        /** How much tokens get added. Default: 20 */
        amountPerBucketRefill: number;
      };
    }> & {
      /** Get token - https://dev.twitch.tv/docs/cli/token-command  */
      twitchToken: string;
      /** Get id  - https://dev.twitch.tv/console/apps/ */
      twitchId: string;
    }
  ) {
    if (config?.respectRateLimit != false)
      // 800 per minute
      this.bucket = new TokenBucket(
        config?.customRateLimit?.bucketCapacity ?? 800,
        config?.customRateLimit?.bucketRefillInterval ?? 60 * 1000,
        config?.customRateLimit?.amountPerBucketRefill ?? 800
      );

    this.controller = new api(
      {
        protocol: "https",
        hostname: "api.twitch.tv",
        path: "/helix",
        headers: {
          Authorization: `Bearer ${config.twitchToken}`,
          "Client-Id": config.twitchId,
        },
      },
      {
        searchChannels: {
          name: "searchChannels",
          suffix: "/search/channels",
          method: "GET",
          // TODO: Figure out a way to make ts allow required values here
          // query is required
          // deno-lint-ignore no-unused-vars
          fn: (config: { query?: string; first?: string }) => {
            return {} as {
              data: {
                broadcaster_language: string;
                broadcaster_login: string;
                display_name: string;
                game_id: string;
                game_name: string;
                id: string;
                is_live: boolean;
                tag_ids: string[];
                thumbnail_url: string;
                title: string;
                started_at: string;
              }[];
              pagination: {
                cursor: string;
              };
            };
          },
        },
        streams: {
          name: "streams",
          suffix: "/streams",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            user_id?: string;
            game_id?: string;
            language?: string;
            user_login?: string;
            after?: string;
            before?: string;
          }) => {
            return {} as {
              data: {
                id: string;
                user_id: string;
                user_login: string;
                user_name: string;
                game_id: string;
                game_name: string;
                type: string;
                title: string;
                viewer_count: number;
                started_at: string;
                language: string;
                thumbnail_url: string;
                tag_ids: string[];
                is_mature: boolean;
              }[];
              pagination: {
                cursor: string;
              };
            };
          },
        },
      }
    );
  }

  async searchChannels(streamerName: string, limit?: number) {
    this.bucket?.tryTake();
    const data = await this.controller.exec("searchChannels", {
      query: streamerName,
      first: limit?.toString(),
    });
    return data;
  }

  async streams(
    config?: AtLeastOne<{
      /** Filter by 1-100 user ids */
      userIds: string[];
      /** Filter by 1-100 user login names */
      userLoginNames: string[];
      /** Filter by 1-100 game ids */
      gameIds: string[];
      /** Filter by 1-100 languages
       *
       * Use ISO 639-1 2 letter codes
       *
       * https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
       */
      languageIds: string[];
      /** Use pagination cursor from previous queries. */
      after: string;
      /** Use pagination cursor from previous queries. */
      before: string;
    }>
  ) {
    if (
      (config?.userIds?.length ?? 0) > 100 ||
      (config?.gameIds?.length ?? 0) > 100 ||
      (config?.languageIds?.length ?? 0) > 100 ||
      (config?.userLoginNames?.length ?? 0) > 100
    )
      throw new Error("You cannot specify more than 100 ids/languages/login names.");

    this.bucket?.tryTake();

    const data = await this.controller.exec("streams", {
      user_id: config?.userIds?.map((i) => i).join("&user_id="),
      user_login: config?.userLoginNames?.map((i) => i).join("&user_login="),
      game_id: config?.gameIds?.map((i) => i).join("&game_id="),
      language: config?.languageIds?.map((i) => i).join("&language="),
      after: config?.after,
      before: config?.before,
    });

    return data;
  }
}

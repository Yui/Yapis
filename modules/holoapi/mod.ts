import { api } from "../../common/api.ts";
export { APIError } from "../../common/api.ts";

export class holoapi {
  private controller;
  constructor() {
    this.controller = new api(
      {
        protocol: "https",
        hostname: "api.holotools.app",
        path: "/v1",
      },
      {
        livestreams: {
          name: "livestreams",
          suffix: "/Live",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: {
            channel_id?: number;
            max_upcoming_hours?: number;
            lookback_hours?: number;
            hide_channel_desc?: number;
          }) => {
            return {
              live: [
                {
                  id: 0,
                  yt_video_key: "",
                  bb_video_id: "",
                  title: "",
                  thumbnail: "",
                  live_schedule: "",
                  live_start: "",
                  live_end: "",
                  live_viewers: "",
                  channel: {
                    id: 0,
                    yt_channel_id: "",
                    bb_space_id: "",
                    name: "",
                    description: "",
                    photo: "",
                    published_at: "",
                    twitter_link: "",
                  },
                },
              ],
              upcoming: [
                {
                  id: 0,
                  yt_video_key: "",
                  bb_video_id: "",
                  title: "",
                  thumbnail: "",
                  live_schedule: "",
                  live_start: "",
                  live_end: "",
                  live_viewers: "",
                  channel: {
                    id: 0,
                    yt_channel_id: "",
                    bb_space_id: "",
                    name: "",
                    description: "",
                    photo: "o",
                    published_at: "",
                    twitter_link: "",
                  },
                },
              ],
              ended: [
                {
                  id: 0,
                  yt_video_key: "",
                  bb_video_id: "",
                  title: "",
                  thumbnail: "",
                  live_schedule: "",
                  live_start: "",
                  live_end: "",
                  live_viewers: "",
                  channel: {
                    id: 0,
                    yt_channel_id: "",
                    bb_space_id: "",
                    name: "",
                    description: "",
                    photo: "",
                    published_at: "",
                    twitter_link: "",
                  },
                },
              ],
              cached: true,
            };
          },
        },
        channels: {
          name: "channels",
          suffix: "/channels",
          method: "GET",
          // deno-lint-ignore no-unused-vars
          fn: (config: { limit?: number; offset?: number; sort?: string; order?: string; name?: string }) => {
            return {} as {
              channels: {
                id: number;
                yt_channel_id: string | null;
                bb_space_id: string | null;
                name: string;
                description: string | null;
                photo: string | null;
                published_at: Date;
                twitter_link: string | null;
                view_count: number;
                subscriber_count: number;
                video_count: number;
              }[];
            };
          },
        },
        channel: {
          name: "channel",
          suffix: "/channels/{id}",
          method: "GET",
          URLParams: true,
          // TODO: Figure out a way to make ts allow required values here
          // deno-lint-ignore no-unused-vars
          fn: (config: { id?: number }) => {
            return {} as {
              id: number;
              yt_channel_id: string | null;
              bb_space_id: string | null;
              name: string;
              description: string | null;
              photo: string | null;
              published_at: Date;
              twitter_link: string | null;
              view_count: number;
              subscriber_count: number;
              video_count: number;
            };
          },
        },
      }
    );
  }

  async live(options?: {
    /** Limit output to a single channel, identified by its HoloAPI ID */
    channelId?: number;
    /** Sometimes members would set up Free Chat rooms. Using this you can constrain how far ahead of now we will return scheduled streams. */
    maxUpcomingHours?: number;
    /** Control how far back in the past you'd like to receive recently-ended videos */
    lookbackHours?: number;
    /** Used to save bandwith. */
    hideChannelDesc?: boolean;
  }) {
    const data = await this.controller.exec("livestreams", {
      channel_id: options?.channelId,
      max_upcoming_hours: options?.maxUpcomingHours,
      lookback_hours: options?.lookbackHours,
      hide_channel_desc: options?.hideChannelDesc == true ? 1 : options?.hideChannelDesc == false ? 0 : undefined,
    });

    if ("message" in data) {
      // @ts-expect-error unexpected
      throw new Error(`API error: ${data.message}`);
    }
    return data;
  }

  async channels(options?: {
    /** The number of channels to return. Default: 25 Must be <= 50 */
    limit?: number;
    /** The number of channels to skip. Default: 0 */
    offset?: number;
    /** Column name to sort by. Default: id */
    sort?: "id" | "name";
    /** Sort order. Default: asc */
    order?: "asc" | "desc";
    /** Channel name search query */
    name?: string;
  }) {
    if (options?.limit && options.limit > 50) options.limit = 50;
    const data = await this.controller.exec("channels", {
      limit: options?.limit,
      offset: options?.offset,
      sort: options?.sort,
      order: options?.order,
      name: options?.name,
    });

    if ("message" in data) {
      // @ts-expect-error unexpected
      throw new Error(`API error: ${data.message}`);
    }
    return data;
  }

  async channel(id: number) {
    const data = await this.controller.exec("channel", { id });

    if ("message" in data) {
      // @ts-expect-error unexpected
      throw new Error(`API error: ${data.message}`);
    }
    return data;
  }
}
